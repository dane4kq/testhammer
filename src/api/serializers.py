from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.models import User
from api.services import CodeService, UserService
from api.validators import phone_validator


class AuthSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=4)
    phone = serializers.CharField(max_length=15, validators=[phone_validator])

    def save(self, **kwargs):
        phone = self.validated_data['phone']
        code = self.validated_data['code']

        code_service = CodeService()
        code_service.check_code(phone, code)

        user_service = UserService()
        return user_service.auth_user(phone=phone)


class UserProfileSerializer(serializers.ModelSerializer):
    invited_users_phones = serializers.SerializerMethodField()

    @swagger_serializer_method(
        serializer_or_field=serializers.CharField(help_text="Телефоны пользователей, который воспользовались ссылкой"))
    def get_invited_users_phones(self, instance):
        return instance.invited_users.all().values_list("phone", flat=True)

    def update(self, instance, validated_data):
        if instance.foreign_invite_code:
            raise ValidationError(detail="Уже ввели реферальный код", code=422)
        if instance == validated_data['foreign_invite_code']:
            raise ValidationError(detail="Нельзя использовать свой реферайльный код", code=422)
        return super().update(instance, validated_data)

    class Meta:
        model = User
        fields = ['id', 'phone', 'self_invite_code', 'invited_users_phones', 'foreign_invite_code']
        read_only_fields = ['invited_users_phones', 'id', 'phone', 'self_invite_code']
