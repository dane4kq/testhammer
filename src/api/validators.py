from django.core.validators import RegexValidator

phone_validator = RegexValidator(regex=r"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$",
                                 message="Невалидный формат телефона")
