from django.urls import path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers

from api.views import UserProfileView, get_phone_code, CustomAuthToken

router = routers.SimpleRouter()

router.register(r"user", UserProfileView, "simple")

schema_view = get_schema_view(
    openapi.Info(
        title="Referal API",
        default_version="v1",
        description="Hammer Systems",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="zanzydnd@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)


urlpatterns = [
    path("get_code/<str:phone>/", get_phone_code, name="get_code"),
    path("auth/", CustomAuthToken.as_view(), name="auth"),
    re_path("swagger(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

urlpatterns += router.urls
