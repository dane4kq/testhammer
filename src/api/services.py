import random
import string
import time

from api.models import VerificationPhoneCode, User


def get_random_six_digits_code():
    return ''.join(random.choices(string.ascii_uppercase +
                                  string.digits, k=6))


class CodeService:
    def generate_code(self) -> int:
        return random.randint(1000, 9999)

    def handle_phone(self, phone: str) -> dict:
        try:
            code_instance = VerificationPhoneCode.objects.get(phone=phone)
        except Exception as e:
            code_instance = VerificationPhoneCode(phone=phone)

        code_instance.code = self.generate_code()
        code_instance.save()

        time.sleep(2)
        return {"phone": phone, "code": code_instance.code}

    def check_code(self, phone: str, code: str):
        VerificationPhoneCode.objects.get(phone=phone, code=code)


class UserService:
    def auth_user(self, phone):
        try:
            user = User.objects.get(phone=phone)
        except:
            user = User(phone=phone, self_invite_code=get_random_six_digits_code())
            user.save()
        return user
