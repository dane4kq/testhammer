from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from api.models.base import BaseModel
from api.validators import phone_validator


class User(BaseModel, AbstractBaseUser):
    phone = models.CharField(max_length=15, unique=True, validators=[phone_validator])
    self_invite_code = models.CharField(max_length=6, unique=True)
    foreign_invite_code = models.ForeignKey("User", to_field="self_invite_code", on_delete=models.CASCADE,
                                            related_name="invited_users", null=True)
    username = None
    password = None
    USERNAME_FIELD = 'phone'


class VerificationPhoneCode(BaseModel):
    code = models.CharField(max_length=4)
    phone = models.CharField(max_length=15, unique=True, validators=[phone_validator])
