from django.http import HttpResponse
from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.models import User
from api.serializers import UserProfileSerializer, AuthSerializer
from api.services import CodeService


@api_view(["GET"])
def get_phone_code(request, phone):
    """Метод для получения кода на телефон. Возвращаю в теле, чтобы проще было вводить."""
    code_service = CodeService()
    return Response(code_service.handle_phone(phone),status=200)


class CustomAuthToken(ObtainAuthToken):
    serializer_class = AuthSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={"request": request})

        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)

        return Response(
            {
                "token" : token.key,
                "phone": user.phone
            }
        )

class OwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.id == request.user.id


class UserProfileView(mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):
    queryset = User.objects.all()
    permission_classes = [OwnerPermission]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserProfileSerializer
