# Hammer Systems тестовое задание

hammer.dane4kq.ru

тг: @dane4kq
email: zanzydnd@gmail.com

## Запуск приложения

Надо создать файл .env.docker в src - пример лежит там же

``
    docker-compose up --build -d
``

## Описание эндпоинтов

```
1. GET /api/get_code/{phone}/ - иммитирует создание телефонного когда(возвращаю в теле , чтобы было проще)
    
2. POST /api/auth/ - body {"phone", "code"} - возвращает token, проставлять в заголовок Authentication: Token {token}

3. GET,PUT,DELETE /api/user/{user_id}/ - GET метод возвращает пользователя и его "реферальщиков", 
                                         PUT - body {"foreign_invite_code"} позволяет вбить чужой реферальный код,
                                         DELETE - удаление      
```

## Docs

hammer.dane4kq.ru/api/redoc/
hammer.dane4kq.ru/api/swagger/